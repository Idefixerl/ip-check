Ein Python Programm zum ermitteln der externen Ip Adresse.

## Voraussetzungen

- Unix/Linux System
- Python 3

## Installieren:

git clone https://Idefixerl@bitbucket.org/Idefixerl/ip-check.git

Ins Verzeichniss wechseln:
``` sh
cd ip-check/
```

Datei ausführbar machen:
``` sh
chmod -x ip-control.py
```
## Benutzung:

```sh
cd ip-check #falls noch nicht im verzeinis
./ip-controll.py
```
Im Benutzerordner wird eine Datei Namens ip.txt angelegt.
Von der Internetseite http://ident.me wird die Internet IP abgefragt und in die ip.txt gespeichert.
Anschließend wird die ip.txt im Terminal ausgegeben.


## Pro-Tipp:

Für eine schöne bunte ausgabe lolcat installieren.
Dann: ```sh
./ip-controll.py | lolcat```
