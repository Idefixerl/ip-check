#!/usr/bin/env python3

import urllib.request
import urllib.parse
import time as zeit
import os


ipadr = ""
uhr = str(zeit.asctime( zeit.localtime(zeit.time())))
schreiben = ''
daten_datei = os.environ['HOME'] + '/' + 'ip.txt'

def ip_get():
    """Hole die IP adresse von ident.me und codiere sie korrekt."""
    para = urllib.parse.urlencode({})
    para = para.encode('ascii')
    with urllib.request.urlopen("http://ident.me", para) as f:

        global ipadr
        ipadr = f.read().decode('utf-8')
    return ipadr

def ip_speichern():
    """speichere die IP adresse in die datei"""
    global ipadr
    with open(daten_datei, "a+") as ip_file:
        ip_file.write('\n' + uhr + '\t' + ipadr)



def last_ip():
    """Liste der IP adressen ausgeben."""
    global schreiben
    while os.path.exists(daten_datei) == False:
        with open(daten_datei, 'w+t') as ip_file:
            ip_file.write('IP Adressliste')


    with open(daten_datei, "rt") as ip_file:
        for zeile in ip_file:
            print(zeile.strip())
            if ipadr in zeile:
                schreiben = 'nein'
            else:
                schreiben = 'ja'


print("IP Adresse wird ermittelt...\n")
ip_get()
last_ip()
if schreiben == 'ja':
    ip_speichern()
print('==================================================')
print('{text: ^40}'.format(text="Check-Zeit: " + uhr))
print('{text: ^40}'.format(text='Aktuelle IP Adresse: '))
print('{text: ^40}'.format(text=ipadr))
print('==================================================')
